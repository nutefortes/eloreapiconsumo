using System.Collections.Generic;
using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{    
    [DataContract]  
    public class AlunoRequestDTO<T>
    {
        public AlunoRequestDTO()
        {
        }

        [DataMember(Name="user")]                 
        public T Aluno { get; set; }

    }
}