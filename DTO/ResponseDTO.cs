using System.Runtime.Serialization;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class ResponseDTO<T>
    {
        public ResponseDTO()
        {
        }
        
        [DataMember(Name="data")]  
        public T Data { get; set; }        
    }
}