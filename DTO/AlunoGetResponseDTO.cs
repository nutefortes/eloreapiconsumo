using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class AlunoGetResponseDTO
    {
        public AlunoGetResponseDTO()
        {
        }
        
        [DataMember(Name="id")]  
        public string ID { get; set; }

        [DataMember(Name="email")]  
        public string Email { get; set; }

        [DataMember(Name="nome")]  
        public string Nome { get; set; }

        [DataMember(Name="sobrenome")]  
        public string Sobrenome { get; set; }
    }
}