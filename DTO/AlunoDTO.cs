using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class AlunoDTO
    {
        public AlunoDTO()
        {
        }

        [Index(0)]
        [DataMember(Name="email")]  
        public string Email { get; set; }

        [Index(1)]
        [DataMember(Name="nome")]  
        public string Nome { get; set; }

        [Index(2)]
        [DataMember(Name="sobrenome")]  
        public string Sobrenome { get; set; }

        [Index(3)]
        [DataMember(Name="cpf")]  
        public string CPF { get; set; }

        [Index(4)]
        [DataMember(Name="uf")]  
        public string UF { get; set; }
        
        [Index(5)]
        [DataMember(Name="telefone")]          
        public string Telefone { get; set; }
        
        [Index(6)]
        [DataMember(Name="tipo_inscricao")]          
        public string TipoInscricao { get; set; }
        
        [Index(7)]
        [DataMember(Name="curso_pacote_id")]          
        public string CursoPacoteID { get; set; }

        [Index(8)]
        [DataMember(Name="gratis")]          
        public bool? Gratis { get; set; }

        [Index(9)]
        [DataMember(Name="curso_valor")]          
        public string CursoValor { get; set; }

    }
}