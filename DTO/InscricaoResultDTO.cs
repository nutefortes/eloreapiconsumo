using System;
using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{
  [DataContract]
  public class InscricaoResultDTO
  {
    public InscricaoResultDTO()
    {
    }

    [DataMember(Name = "id")]
    public int Id { get; set; }

    [DataMember(Name = "user_id")]
    public int UserId { get; set; }


    [DataMember(Name = "user_email")]
    public string Email { get; set; }

    [DataMember(Name = "user_nome")]
    public string Nome { get; set; }

    [DataMember(Name = "status")]
    public string Status { get; set; }

    [DataMember(Name = "valor_pago")]
    public decimal? ValorPago { get; set; }

    [DataMember(Name = "efetivada_em")]
    public DateTime? dtEfetivacao { get; set; }

    [DataMember(Name = "created_at")]
    public DateTime dtCriacao { get; set; }
  }
}