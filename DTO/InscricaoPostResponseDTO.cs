using System.Collections.Generic;
using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{    
    [DataContract]  
    public class InscricaoPostResponseDTO
    {
        public InscricaoPostResponseDTO()
        {
        }

        [DataMember(Name="id")]  
        public string ID { get; set; }
                
        [DataMember(Name="valor")]  
        public decimal Valor { get; set; }
                
        [DataMember(Name="user_email")]  
        public string Email { get; set; }
    }
}