using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class CursoGetResponseDTO
    {
        public CursoGetResponseDTO()
        {
        }
        
        [DataMember(Name="id")]  
        public string ID { get; set; }

        [DataMember(Name="slug")]  
        public string Slug { get; set; }

        [DataMember(Name="nome_curso")]  
        public string NomeCurso { get; set; }

        [DataMember(Name="nome_turma")]  
        public string NomeTurma { get; set; }     

        [DataMember(Name="categoria")]  
        public string Categoria { get; set; }     

        [DataMember(Name="tipo")]  
        public string Tipo { get; set; }     
    }
}