using System.Runtime.Serialization;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class InscricaoDTO
    {
        public InscricaoDTO()
        {
          Notificar=false;
        }

        public InscricaoDTO(InscricaoDTO inscricaoDTO)
        {
            Id = inscricaoDTO.Id;
            UserId = inscricaoDTO.UserId;
            Email=inscricaoDTO.Email;
            Notificar=inscricaoDTO.Notificar;            
        }

        [DataMember(Name="id")]  
        public int Id { get; set; }
        
        [DataMember(Name="user_id")]  
        public int UserId { get; set; }        

        [DataMember(Name="user_email")]  
        public string Email { get; set; }

        [DataMember(Name="notificar")]  
        public bool Notificar { get; set; }
    }
}