using System.Collections.Generic;
using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class PacoteGetResponseDTO
    {
        public PacoteGetResponseDTO()
        {
        }
        
        [DataMember(Name="id")]  
        public string ID { get; set; }

        [DataMember(Name="slug")]  
        public string Slug { get; set; }

        [DataMember(Name="nome")]  
        public string Nome { get; set; }       

        [DataMember(Name="turmas")]  
        public List<CursoGetResponseDTO> Turmas { get; set; }       
    }
}