using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Elore_ConsumoAPI.DTO
{    
    [DataContract]  
    public class TagDTO
    {
        public TagDTO()
        {
        }

        [DataMember(Name="tags")]                 
        public List<string> tags { get; set; }

    }
}