using System.Runtime.Serialization;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class InscricaoCursoDTO : InscricaoDTO
    {                
        public InscricaoCursoDTO(InscricaoDTO inscricaoDTO) : base(inscricaoDTO){}
        
        [DataMember(Name="valor")]  
        public decimal? Valor { get; set; }        
    }
}