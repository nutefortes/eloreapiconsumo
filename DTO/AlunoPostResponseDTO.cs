using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class AlunoPostResponseDTO
    {
        public AlunoPostResponseDTO()
        {
        }
        
        [DataMember(Name="id")]  
        public string ID { get; set; }

        [Index(0)]
        [DataMember(Name="email")]  
        public string Email { get; set; }


        [DataMember(Name="cpf")]  
        public string CPF { get; set; }
    }
}