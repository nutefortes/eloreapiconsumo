
using System.Runtime.Serialization;

namespace Elore_ConsumoAPI.DTO
{    
    [DataContract]  
    public class EchoDTO
    {
        
        [DataMember(Name="organizacao_permalink")]  
        public string OrganizacaoPermalink { get; set; }
        
        [DataMember(Name="organizacao_nome")]  
        public string OrganizacaoNome { get; set; }
    }
}