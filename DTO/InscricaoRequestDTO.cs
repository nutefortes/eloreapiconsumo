using System.Collections.Generic;
using System.Runtime.Serialization;
using CsvHelper.Configuration.Attributes;

namespace Elore_ConsumoAPI.DTO
{    
    [DataContract]  
    public class InscricaoRequestDTO<T>
    {
        public InscricaoRequestDTO()
        {
        }

        [DataMember(Name="inscricao")]                 
        public T Inscricao { get; set; }

    }
}