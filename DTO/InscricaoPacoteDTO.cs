using System.Runtime.Serialization;

namespace Elore_ConsumoAPI.DTO
{
    [DataContract]  
    public class InscricaoPacoteDTO : InscricaoDTO
    {
        public InscricaoPacoteDTO()
        {
          Gratuita=false;
        }

        public InscricaoPacoteDTO(InscricaoDTO inscricaoDTO) : base(inscricaoDTO){}
            
        [DataMember(Name="gratuita")]  
        public bool Gratuita { get; set; }
    }
}