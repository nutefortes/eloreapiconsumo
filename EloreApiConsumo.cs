using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Elore_ConsumoAPI.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Elore_ConsumoAPI
{
  public static class EloreApiConsumo
  {
    private static string baseUri = "https://api.elore.com.br/api/";

    static HttpClient client;

    public static async Task<EchoDTO> Echo(string eloreToken)
    {
      string rota = "auth/echo";

      var serializer = new DataContractJsonSerializer(typeof(EchoDTO));

      client = new HttpClient();

      client.AddToken(eloreToken);

      using (var request = new HttpRequestMessage(HttpMethod.Get, $"{baseUri}{rota}"))
      using (var response = await client.SendAsync(request))
      {
        var content = await response.Content.ReadAsStringAsync();

        if (response.IsSuccessStatusCode == false)
        {
          //Trate aqui suas exceções
          throw new Exception($"Status Code: {(int)response.StatusCode} | Mensagem: {content}");
        }
        var jsonSerializeSettings = new JsonSerializerSettings();
        return JsonConvert.DeserializeObject<EchoDTO>(content);
      }
    }

    public static async Task<ResponseDTO<List<AlunoGetResponseDTO>>> RetornarUsuarioPorEmail(string email, string eloreToken)
    {
      return await RetornarUsuario(email, null, null, eloreToken);
    }

    public static async Task<ResponseDTO<List<AlunoGetResponseDTO>>> RetornarUsuarioPorTag(string tag, string eloreToken)
    {
      return await RetornarUsuario(null, null, tag, eloreToken);
    }

    public static async Task<ResponseDTO<List<AlunoGetResponseDTO>>> RetornarUsuarioPorCPF(string cpf, string eloreToken)
    {
      return await RetornarUsuario(null, cpf, null, eloreToken);
    }

    private static async Task<ResponseDTO<List<AlunoGetResponseDTO>>> RetornarUsuario(string email, string cpf, string tag, string eloreToken)
    {
      string query = "";
      if (!string.IsNullOrWhiteSpace(email))
      {
        query += $"email={email}&";
      }
      if (!string.IsNullOrWhiteSpace(cpf))
      {
        query += $"cpf={cpf}&";
      }
      if (!string.IsNullOrWhiteSpace(tag))
      {
        query += $"tags={tag}&";
      }
      if (query.Length > 0)
        query.Remove(query.Length - 1);
      string rota = "users";
      var serializer = new DataContractJsonSerializer(typeof(List<AlunoDTO>));

      client = new HttpClient();
      client.AddToken(eloreToken);

      using (var request = new HttpRequestMessage(HttpMethod.Get, $"{baseUri}{rota}?{query}"))
      using (var response = await client.SendAsync(request))
      {
        var content = await response.Content.ReadAsStringAsync();

        if (response.IsSuccessStatusCode == false)
        {
          //Trate aqui suas exceções
          throw new Exception($"Status Code: {(int)response.StatusCode} | Mensagem: {content}");
        }
        var jsonSerializeSettings = new JsonSerializerSettings();
        return JsonConvert.DeserializeObject<ResponseDTO<List<AlunoGetResponseDTO>>>(content);
      }
    }

    public static async Task<ResponseDTO<List<CursoGetResponseDTO>>> RetornarCursos(string eloreToken)
    {
      string rota = "cursos";

      return await ExecApiOperation<ResponseDTO<List<CursoGetResponseDTO>>>(rota, eloreToken);
    }

    public static async Task<ResponseDTO<CursoGetResponseDTO>> RetornarCursoPorID(string ID, string eloreToken)
    {
      string rota = $"cursos/{ID}";

      return await ExecApiOperation<ResponseDTO<CursoGetResponseDTO>>(rota, eloreToken);
    }

    public static async Task<ResponseDTO<List<CursoGetResponseDTO>>> RetornarCursoPorSlug(string slug, string eloreToken)
    {
      string rota = $"cursos?slug={slug}";

      return await ExecApiOperation<ResponseDTO<List<CursoGetResponseDTO>>>(rota, eloreToken);
    }

    public static async Task<ResponseDTO<PacoteGetResponseDTO>> RetornarPacotePorID(string ID, string eloreToken)
    {
      string rota = $"pacotes/{ID}";

      return await ExecApiOperation<ResponseDTO<PacoteGetResponseDTO>>(rota, eloreToken);
    }

    public static async Task<ResponseDTO<List<PacoteGetResponseDTO>>> RetornarPacotePorSlug(string slug, string eloreToken)
    {
      string rota = $"pacotes?slug={slug}";

      return await ExecApiOperation<ResponseDTO<List<PacoteGetResponseDTO>>>(rota, eloreToken);
    }

    public static async Task<ResponseDTO<List<PacoteGetResponseDTO>>> RetornarPacotes(string eloreToken)
    {
      string rota = "pacotes";

      return await ExecApiOperation<ResponseDTO<List<PacoteGetResponseDTO>>>(rota, eloreToken);
    }


    public static async Task<ResponseDTO<List<InscricaoResultDTO>>> RetornarInscritosPorCurso(string curso_id, string eloreToken, bool somenteInscricoesAtivas = true, int pagina = 1, int itens_por_pagina = 50)
    {
      return await RetornarInscritos(TipoConteudo.Curso, curso_id, eloreToken,somenteInscricoesAtivas, pagina, itens_por_pagina);
    }

    public static async Task<ResponseDTO<List<InscricaoResultDTO>>> RetornarInscritosPorPacote(string pacote_id, string eloreToken, bool somenteInscricoesAtivas = true, int pagina = 1, int itens_por_pagina = 50)
    {
      return await RetornarInscritos(TipoConteudo.Pacote, pacote_id, eloreToken, somenteInscricoesAtivas, pagina, itens_por_pagina);
    }

    private static async Task<ResponseDTO<List<InscricaoResultDTO>>> RetornarInscritos(TipoConteudo tipoConteudo, string conteudo_id, string eloreToken, bool somenteInscricoesAtivas = true, int pagina = 1, int itens_por_pagina = 50)
    {
      string queryTipoConteudo = "cursos";
      if (tipoConteudo == TipoConteudo.Pacote)
        queryTipoConteudo = "pacotes";

      string rota = $"{queryTipoConteudo}/{conteudo_id}/inscricoes?page={pagina}&per_page={itens_por_pagina}&ativas={somenteInscricoesAtivas}";

      client = new HttpClient();
      client.AddToken(eloreToken);

      using (var request = new HttpRequestMessage(HttpMethod.Get, $"{baseUri}{rota}"))
      using (var response = await client.SendAsync(request))
      {
        var content = await response.Content.ReadAsStringAsync();

        if (response.IsSuccessStatusCode == false)
        {
          //Trate aqui suas exceções
          throw new Exception($"Status Code: {(int)response.StatusCode} | Mensagem: {content}");
        }
        var jsonSerializeSettings = new JsonSerializerSettings();
        return JsonConvert.DeserializeObject<ResponseDTO<List<InscricaoResultDTO>>>(content);
      }
    }

    public static async Task<ResponseDTO<List<string>>> AdicionarTag(int aluno_ID, string tag, string eloreToken)
    {
      string rota = $"users/{aluno_ID}/tags";
      var serializer = new DataContractJsonSerializer(typeof(List<AlunoDTO>));

      client = new HttpClient();
      client.AddToken(eloreToken);

      var tagDTO = new TagDTO() { tags = new List<string> { tag } };
      using (var request = new HttpRequestMessage(HttpMethod.Post, $"{baseUri}{rota}"))
      {
        var jsonTagRequest = JsonConvert.SerializeObject(tagDTO);
        request.Content = new StringContent(jsonTagRequest, Encoding.UTF8, "application/json");

        using (var response = await client.SendAsync(request))
        {
          var content = await response.Content.ReadAsStringAsync();

          if (response.IsSuccessStatusCode == false)
          {
            //Trate aqui suas exceções
            throw new Exception($"ADIÇÃO DE TAG - Status Code: {(int)response.StatusCode} | Mensagem: {content}");
          }
          Console.Write($"ADIÇÃO DE TAG - {response.StatusCode} | Id do Aluno: {aluno_ID}");
          var jsonSerializeSettings = new JsonSerializerSettings();
          return JsonConvert.DeserializeObject<ResponseDTO<List<string>>>(content);
        }
      }
    }

    public static async Task<ResponseDTO<List<string>>> RemoverTag(int aluno_ID, string tag, string eloreToken)
    {
      string rota = $"users/{aluno_ID}/tags/{tag}";
      var serializer = new DataContractJsonSerializer(typeof(List<AlunoDTO>));

      client = new HttpClient();
      client.AddToken(eloreToken);

      using (var request = new HttpRequestMessage(HttpMethod.Delete, $"{baseUri}{rota}"))
      {
        using (var response = await client.SendAsync(request))
        {
          var content = await response.Content.ReadAsStringAsync();

          if (response.IsSuccessStatusCode == false)
          {
            //Trate aqui suas exceções
            throw new Exception($"REMOÇÃO DE TAG - Status Code: {(int)response.StatusCode} | Mensagem: {content}");
          }
          Console.Write($"REMOÇÃO DE TAG - {response.StatusCode} | Id do Aluno: {aluno_ID}");
          var jsonSerializeSettings = new JsonSerializerSettings();
          return JsonConvert.DeserializeObject<ResponseDTO<List<string>>>(content);
        }
      }
    }

    public static async Task<ResponseDTO<AlunoPostResponseDTO>> CadastrarAluno(AlunoRequestDTO<AlunoDTO> aluno, string eloreToken)
    {
      string rota = "users";
      var serializer = new DataContractJsonSerializer(typeof(List<AlunoDTO>));

      client = new HttpClient();
      client.AddToken(eloreToken);

      using (var request = new HttpRequestMessage(HttpMethod.Post, $"{baseUri}{rota}"))
      {
        var jsonAlunoRequest = JsonConvert.SerializeObject(aluno);
        request.Content = new StringContent(jsonAlunoRequest, Encoding.UTF8, "application/json");

        using (var response = await client.SendAsync(request))
        {
          var content = await response.Content.ReadAsStringAsync();

          if (response.IsSuccessStatusCode == false)
          {
            //Trate aqui suas exceções
            throw new Exception($"CADASTRO - Status Code: {(int)response.StatusCode} | Mensagem: {content}");
          }
          Console.Write($"CADASTRO - {response.StatusCode}");
          var jsonSerializeSettings = new JsonSerializerSettings();
          return JsonConvert.DeserializeObject<ResponseDTO<AlunoPostResponseDTO>>(content);
        }
      }
    }


    public static async Task<ResponseDTO<InscricaoPostResponseDTO>> InscreverAlunoEmCurso(InscricaoRequestDTO<InscricaoDTO> inscricao, string curso_id, string eloreToken)
    {
      string rota = $"cursos/{curso_id}/inscricoes";
      var serializer = new DataContractJsonSerializer(typeof(List<InscricaoDTO>));

      client = new HttpClient();
      client.AddToken(eloreToken);

      using (var request = new HttpRequestMessage(HttpMethod.Post, $"{baseUri}{rota}"))
      {
        var jsonInscricaoRequest = JsonConvert.SerializeObject(inscricao);
        request.Content = new StringContent(jsonInscricaoRequest, Encoding.UTF8, "application/json");

        using (var response = await client.SendAsync(request))
        {
          var content = await response.Content.ReadAsStringAsync();

          if (response.IsSuccessStatusCode)
          {
            Console.Write($"INSCRIÇÃO - {response.StatusCode}");
          }
          else if (response.StatusCode == System.Net.HttpStatusCode.UnprocessableEntity)
          {
            Console.Write($"INSCRIÇÃO NÃO REALIZADA - Motivo: {content}");
          }
          else
          {
            //Trate aqui suas exceções
            throw new Exception($" ---->> INSCRIÇÃO - Status Code: {(int)response.StatusCode} | Mensagem: {content}");
          }

          var jsonSerializeSettings = new JsonSerializerSettings();
          return JsonConvert.DeserializeObject<ResponseDTO<InscricaoPostResponseDTO>>(content);
        }
      }
    }


    public static async Task<ResponseDTO<InscricaoPostResponseDTO>> InscreverAlunoEmPacote(InscricaoRequestDTO<InscricaoDTO> inscricao, string pacote_id, string eloreToken, bool notificar_por_email = false)
    {
      string rota = $"pacotes/{pacote_id}/inscricoes";
      var serializer = new DataContractJsonSerializer(typeof(List<InscricaoDTO>));

      client = new HttpClient();
      client.AddToken(eloreToken);

      using (var request = new HttpRequestMessage(HttpMethod.Post, $"{baseUri}{rota}"))
      {
        var jsonInscricaoRequest = JsonConvert.SerializeObject(inscricao);
        request.Content = new StringContent(jsonInscricaoRequest, Encoding.UTF8, "application/json");

        using (var response = await client.SendAsync(request))
        {
          var content = await response.Content.ReadAsStringAsync();

          if (response.IsSuccessStatusCode)
          {
            Console.Write($"INSCRIÇÃO - {response.StatusCode}");
          }
          else if (response.StatusCode == System.Net.HttpStatusCode.UnprocessableEntity)
          {
            Console.Write($"INSCRIÇÃO NÃO REALIZADA - Motivo: {content}");
          }
          else
          {
            //Trate aqui suas exceções
            throw new Exception($" ---->> INSCRIÇÃO - Status Code: {(int)response.StatusCode} | Mensagem: {content}");
          }

          var jsonSerializeSettings = new JsonSerializerSettings();
          return JsonConvert.DeserializeObject<ResponseDTO<InscricaoPostResponseDTO>>(content);
        }
      }
    }

    private static async Task<T> ExecApiOperation<T>(string rota, string eloreToken)
    {
      client = new HttpClient();
      client.AddToken(eloreToken);

      using (var request = new HttpRequestMessage(HttpMethod.Get, $"{baseUri}{rota}"))
      using (var response = await client.SendAsync(request))
      {
        var content = await response.Content.ReadAsStringAsync();

        if (response.IsSuccessStatusCode == false)
        {
          //Trate aqui suas exceções
          throw new Exception($"Status Code: {(int)response.StatusCode} | Mensagem: {content}");
        }
        var jsonSerializeSettings = new JsonSerializerSettings();
        return JsonConvert.DeserializeObject<T>(content);
      }
    }


    private static void AddToken(this HttpClient client, string eloreToken)
    {
      client.DefaultRequestHeaders.Accept.Clear();
      client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      client.DefaultRequestHeaders.Add("X-Auth-Token", eloreToken);
    }

    private static T Deserialize<T>(string json, bool ignoreRoot) where T : class
    {
      return ignoreRoot
          ? JObject.Parse(json)?.Properties()?.First()?.Value?.ToObject<T>()
          : JObject.Parse(json)?.ToObject<T>();
    }

  }
}