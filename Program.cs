﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using CsvHelper;
using Elore_ConsumoAPI.DTO;
using Newtonsoft.Json;

namespace Elore_ConsumoAPI
{
  class Program
  {
    private static string eloreToken = "eloreToken****************************"; //Admin > Integrações > API Token
    private static List<CursoGetResponseDTO> cursos;

    private static List<PacoteGetResponseDTO> pacotes;

    static void Main(string[] args)
    {
      Console.WriteLine("Consumindo a API do Elore!");

      //TESTANDO TOKEN            
      var sucessoTeste = TestarToken(eloreToken);


      //LENDO CSV                  
      string nome_csv = "csv_para_import.csv";
      var alunos = LerCSV(nome_csv, false);

      //CHECANDO SE ALUNOS E CURSOS/PACOTES EXISTEM
      ConsultarAlunosECursosPacotes(alunos);

      //CADASTRANDO E MATRICULANDO ALUNOS DO CSV
      CadastrarEInscreverAlunos(alunos, false);



      //IMPRIMINDO A LISTA DE TODOS OS CURSOS NO ELORE
      // ImprimirListaCursos();


      #region CONSULTANDO TOTAL DE INSCRITOS NO CURSO
      // var curso_id = "ID_DO_CURSO";
      // var curso_slug = "SLUG_DO_CURSO";
      // var totalInscritosNoCurso = RetornarTotalInscritosNoCurso(ConsultaPor.Slug, curso_slug, true);
      // Console.WriteLine($"Total de inscritos no curso: {totalInscritosNoCurso}");
      #endregion

      #region ADICIONAR TAG PARA INSCRITOS NO CURSO
      // var curso_id = "ID_DO_CURSO";
      // var tag_para_adicionar = "aluno-manha";
      // var totalAdicionado = AdicionarTagParaInscritosNoCurso(tag_para_adicionar, curso_id);
      // Console.WriteLine($"Total de inscritos que tiveram a tag adicionada: {totalAdicionado}");
      #endregion

      #region LISTAR USUÁRIOS CONTENDO A TAG
      // var tag_para_consulta = "aluno-turma-manha";
      // ImprimirUsuariosComTag(tag_para_consulta);
      #endregion

      //IMPRIMINDO A LISTA DE TODOS OS PACOTES NO ELORE
      // ImprimirListaPacotes();


      #region CONSULTANDO TOTAL DE INSCRITOS NO PACOTE
      // var pacote_id = "ID_DO_PACOTE";
      // var pacote_slug = "SLUG_DO_PACOTE";
      // var totalInscritosNoPacote = RetornarTotalInscritosNoPacote(ConsultaPor.Slug, pacote_slug, true);
      // Console.WriteLine($"Total de inscritos no pacote: {totalInscritosNoPacote}");
      #endregion

      #region INSCREVER NO PACOTE
      // var pacote_id = ID_DO_PACOTE;
      // var user_id = ID_DO_USUARIO;
      // var email = "email_do_aluno";
      // var Inscricao = new InscricaoDTO
      // {
      //   UserId = int.Parse(user_id),
      //   Email = email,
      //   Valor = 0
      // };
      // var inscricaoRequestDTO = new InscricaoRequestDTO<InscricaoDTO> { Inscricao = Inscricao};
      // var retorno = EloreApiConsumo.InscreverAlunoEmPacote(inscricaoRequestDTO,pacote_id,eloreToken,true).Result;
      // Console.WriteLine($"Inscricao efetivada com sucesso!");      
      #endregion
    }

    private static void ImprimirUsuariosComTag(string tag_para_consultar)
    {
      var usuarios = EloreApiConsumo.RetornarUsuarioPorTag(tag_para_consultar, eloreToken).Result.Data;

      usuarios.ForEach(usuario =>
    {
      Console.WriteLine($"Id: {usuario.ID} | Email: {usuario.Email} | Nome: {usuario.Nome} {usuario.Sobrenome}");
    });
      Console.WriteLine($"**Total de usuario com a tag {tag_para_consultar}: {usuarios.Count}**");
    }

    private static bool TestarToken(string eloreToken)
    {
      var echo = EloreApiConsumo.Echo(eloreToken).Result;
      Console.WriteLine($"Autenticado com sucesso! Organização: {echo.OrganizacaoNome} | Permalink: {echo.OrganizacaoPermalink}");
      return echo.OrganizacaoPermalink != null;
    }

    private static List<AlunoDTO> LerCSV(string nome_csv, bool exibirDadosTurmaPacote = false)
    {
      Console.WriteLine("##########################################");
      Console.WriteLine("######## LENDO CSV - INÍCIO ##########");
      Console.WriteLine("##########################################");
      var file = Path.Combine("csv_alunos", nome_csv);
      var full_path = Path.GetFullPath(file);
      var alunos = new List<AlunoDTO>();
      using (var reader = new StreamReader(full_path))
      using (var csv = new CsvReader(reader))
      {
        csv.Configuration.Delimiter = ";";
        csv.Configuration.MissingFieldFound = null;
        csv.Configuration.TrimOptions = CsvHelper.Configuration.TrimOptions.Trim;
        var records = csv.GetRecords<AlunoDTO>();
        int i = 0;

        foreach (var aluno in records)
        {
          //Qualquer tratamento que precise ser feito na lista, um bom momento é aqui
          Console.WriteLine($"Aluno {++i} - Email: {aluno.Email} | Nome: {aluno.Nome} | Sobrenome: {aluno.Sobrenome} | CPF: {aluno.CPF} | UF: {aluno.UF}");
          Console.WriteLine($">> Dados Inscr. Tipo: {aluno.TipoInscricao} | ID Turma/Pacote: {aluno.CursoPacoteID} | Grátis: {aluno.Gratis} | Valor(apenas curso): {aluno.CursoValor} | UF: {aluno.UF}");
          alunos.Add(aluno);
        }
      }
      Console.WriteLine("#######################################");
      Console.WriteLine("######## LENDO CSV - FIM ##########");
      Console.WriteLine("#######################################");
      Console.WriteLine();
      return alunos;
    }

    private static void ConsultarAlunosECursosPacotes(List<AlunoDTO> alunos)
    {
      Console.WriteLine("###############################################################");
      Console.WriteLine("######## CONSULTANDO ALUNOS E CURSOS/PACOTES NO CSV - INÍCIO ##########");
      Console.WriteLine("###############################################################");
      var posicao = 0;
      cursos = EloreApiConsumo.RetornarCursos(eloreToken).Result.Data;
      pacotes = EloreApiConsumo.RetornarPacotes(eloreToken).Result.Data;
      foreach (var aluno in alunos) //Iterando em todos os alunos do CSV
      {
        validarDadosAluno(aluno);
        var alunoCadastrado = EloreApiConsumo.RetornarUsuarioPorEmail(aluno.Email, eloreToken).Result;
        if (alunoCadastrado.Data.Count == 0)
        {
          Console.WriteLine($"{++posicao} - Email: {aluno.Email} não localizado. Será feito cadastro.");
        }
        else
        {
          var primeiroAluno = alunoCadastrado.Data.Find(a => a.Email == aluno.Email);
          Console.WriteLine($"{++posicao} - Aluno encontrado! ID: {primeiroAluno.ID} Email: {primeiroAluno.Email} | Nome: {primeiroAluno.Nome} |  Sobrenome: {primeiroAluno.Sobrenome}");
        }

        //Procurando curso, caso passado como parâmetro                    
        if (string.IsNullOrEmpty(aluno.CursoPacoteID))
          Console.WriteLine($"----->>> Nenhum curso informado");
        else
        {
          bool achouCursoPacote = false;
          PacoteGetResponseDTO pacoteEncontrado;
          CursoGetResponseDTO cursoEncontrado;
          string nomeCursoPacote = "";
          if (aluno.TipoInscricao == "pacote")
          {
            pacoteEncontrado = pacotes.Find(c => c.ID == aluno.CursoPacoteID);
            achouCursoPacote = pacoteEncontrado != null;
            nomeCursoPacote = achouCursoPacote ? pacoteEncontrado.Nome : null;
          }
          else
          {
            cursoEncontrado = cursos.Find(c => c.ID == aluno.CursoPacoteID);
            achouCursoPacote = cursoEncontrado != null;
            nomeCursoPacote = achouCursoPacote ? cursoEncontrado.NomeCurso : null;
          }

          if (achouCursoPacote)
            Console.WriteLine($"----->>> [OK] {aluno.TipoInscricao} com ID {aluno.CursoPacoteID} encontrado! {aluno.TipoInscricao}: {nomeCursoPacote}. Email que tentaremos matricular: {aluno.Email}");
          else
            Console.WriteLine($"----->>> {aluno.TipoInscricao} com ID {aluno.CursoPacoteID} NÃO encontrado. Não será feito inscrição!");
        }
      }
      Console.WriteLine("#############################################################");
      Console.WriteLine("######## CONSULTANDO ALUNOS E CURSOS NO CSV - FIM ##########");
      Console.WriteLine("#############################################################");
      Console.WriteLine();
    }

    private static void validarDadosAluno(AlunoDTO aluno)
    {
      aluno.TipoInscricao = string.IsNullOrWhiteSpace(aluno.TipoInscricao) ? string.Empty : aluno.TipoInscricao.ToLower();
      var tipoInscricao = string.IsNullOrEmpty(aluno.TipoInscricao) ? string.Empty : aluno.TipoInscricao;
      var deveInscrever = tipoInscricao != string.Empty;
      if (deveInscrever && !(tipoInscricao == "curso" || tipoInscricao == "pacote"))
        throw new InvalidDataException($"ERRO: Valor inválido para a coluna Tipo Inscricao. Email {aluno.Email}. Deixe em branco ou informe turma|pacote. Valor informado: {aluno.TipoInscricao}");

      if (deveInscrever && string.IsNullOrWhiteSpace(aluno.CursoPacoteID))
        throw new InvalidDataException($"ERRO: Id do {tipoInscricao} não informado para o Email: {aluno.Email}");

      if (deveInscrever && !aluno.Gratis.HasValue)
        throw new InvalidDataException($"ERRO: Não foi informado se a inscrição gerada será grátis ou não para o Email: {aluno.Email}");

      if (tipoInscricao == "curso" && !aluno.Gratis.Value && string.IsNullOrWhiteSpace(aluno.CursoValor))
        throw new InvalidDataException($"ERRO: Não foi informado o valor para uma inscrição em curso pago. Email: {aluno.Email}");
    }

    private static void CadastrarEInscreverAlunos(List<AlunoDTO> alunos, bool somenteCadastrar = false)
    {
      Console.WriteLine("###############################################################");
      Console.WriteLine("######## CADASTRANDO E INSCREVENDO ALUNOS - INÍCIO ##########");
      Console.WriteLine("###############################################################");
      int posicao = 0;
      foreach (var aluno in alunos)
      {
        //Cadastrar alunos                
        Console.Write($"{++posicao} - ");
        var alunoRequestDTO = new AlunoRequestDTO<AlunoDTO> { Aluno = aluno };
        var alunoCadastrado = EloreApiConsumo.CadastrarAluno(alunoRequestDTO, eloreToken).Result;
        Console.WriteLine($" - ID Aluno: {alunoCadastrado.Data.ID} | Email: {alunoCadastrado.Data.Email} | CPF: {alunoCadastrado.Data.CPF}");

        if (!somenteCadastrar)
        {
          //Inscrever alunos
          if (string.IsNullOrEmpty(aluno.CursoPacoteID))
            Console.Write($"{posicao} - {aluno.TipoInscricao} não informado no CSV");

          else if (aluno.TipoInscricao == "pacote" ? !pacotes.Exists(p => p.ID == aluno.CursoPacoteID) :
          !cursos.Exists(c => c.ID == aluno.CursoPacoteID))
            Console.Write($"{posicao} - [ERRO] {aluno.TipoInscricao} com ID {aluno.CursoPacoteID} não localizado na lista de {aluno.TipoInscricao}(s) da organização");
          else
          {
            Console.Write($"{posicao} - ");
            var inscricaoRequestDTO = new InscricaoRequestDTO<InscricaoDTO> { Inscricao = new InscricaoDTO { UserId = int.Parse(alunoCadastrado.Data.ID), Email = alunoCadastrado.Data.Email } };
            Task<ResponseDTO<InscricaoPostResponseDTO>> inscricaoResponseDTO;
            if (aluno.TipoInscricao == "curso")
            {
              inscricaoRequestDTO.Inscricao = new InscricaoCursoDTO(inscricaoRequestDTO.Inscricao);
              ((InscricaoCursoDTO)inscricaoRequestDTO.Inscricao).Valor = aluno.Gratis.Value ? 0 : decimal.Parse(aluno.CursoValor);
              inscricaoResponseDTO = EloreApiConsumo.InscreverAlunoEmCurso(inscricaoRequestDTO, aluno.CursoPacoteID, eloreToken);
            }
            else
            {
              inscricaoRequestDTO.Inscricao = new InscricaoPacoteDTO(inscricaoRequestDTO.Inscricao);
              ((InscricaoPacoteDTO)inscricaoRequestDTO.Inscricao).Gratuita = aluno.Gratis.Value;
              inscricaoResponseDTO = EloreApiConsumo.InscreverAlunoEmPacote(inscricaoRequestDTO, aluno.CursoPacoteID, eloreToken);
            }
            var inscricao = inscricaoResponseDTO.Result;
            if (inscricao.Data != null)
              Console.WriteLine($" - ID Inscrição: {inscricao.Data.ID} | Email: {inscricao.Data.Email}");
          }
        }
        Console.WriteLine("\r\n");
      }

      Console.WriteLine("#############################################################");
      Console.WriteLine("######## CADASTRANDO E INSCREVENDO ALUNOS - FIM ##########");
      Console.WriteLine("#############################################################");
    }

    private static int RetornarTotalInscritosNoCurso(ConsultaPor consultaPor, string valor_consulta, bool imprimirInscritos = false)
    {
      CursoGetResponseDTO cursoBuscado = null;
      if (consultaPor == ConsultaPor.ID)
        cursoBuscado = EloreApiConsumo.RetornarCursoPorID(valor_consulta, eloreToken).Result.Data;
      else
      {
        var result = EloreApiConsumo.RetornarCursoPorSlug(valor_consulta, eloreToken).Result;
        if (result.Data != null)
          cursoBuscado = result.Data[0];
      }
      if (cursoBuscado == null)
      {
        Console.WriteLine($"Curso não localizado. Tipo de Consulta: {consultaPor} | Valor da Consulta: {valor_consulta}");
        return 0;
      }
      var InscritosNoCurso = new List<InscricaoResultDTO>();

      //Recuperando todos os alunos inscritos
      var temMaisAlunos = true;
      var pagina = 1;
      do
      {
        var inscritos = EloreApiConsumo.RetornarInscritosPorCurso(cursoBuscado.ID, eloreToken, pagina: pagina).Result.Data;
        if (inscritos.Count > 0)
        {
          pagina++;
          InscritosNoCurso.AddRange(inscritos);
        }
        else
          temMaisAlunos = false;
      } while (temMaisAlunos);

      int incrementoAluno = 0;
      if (imprimirInscritos)
        InscritosNoCurso.ForEach(inscrito =>
        {
          Console.WriteLine($"{++incrementoAluno}. ID Inscrição: {inscrito.Id} | ID User: {inscrito.UserId} | Nome/Email: {inscrito.Nome}({inscrito.Email}) Status: {inscrito.Status} | Valor Pago: {inscrito.ValorPago} | Dt Efetivacao: {inscrito.dtEfetivacao} | Dt Criação: {inscrito.dtCriacao}\n");
        });

      return InscritosNoCurso.Count;
    }


    private static int RetornarTotalInscritosNoPacote(ConsultaPor consultaPor, string valor_consulta, bool imprimirInscritos = false)
    {
      PacoteGetResponseDTO pacoteBuscado = null;
      if (consultaPor == ConsultaPor.ID)
        pacoteBuscado = EloreApiConsumo.RetornarPacotePorID(valor_consulta, eloreToken).Result.Data;
      else
      {
        var result = EloreApiConsumo.RetornarPacotePorSlug(valor_consulta, eloreToken).Result;
        if (result != null)
          pacoteBuscado = result.Data[0];
      }

      if (pacoteBuscado == null)
      {
        Console.WriteLine($"Pacote não localizado. Tipo de Consulta: {consultaPor} | Valor da Consulta: {valor_consulta}");
        return 0;
      }

      var InscritosNoPacote = EloreApiConsumo.RetornarInscritosPorPacote(pacoteBuscado.ID, eloreToken).Result.Data;
      int incrementoAluno = 0;
      if (imprimirInscritos)
        InscritosNoPacote.ForEach(inscrito =>
        {
          Console.WriteLine($"{++incrementoAluno}. ID Inscrição: {inscrito.Id} | ID User: {inscrito.UserId} | Nome/Email: {inscrito.Nome}({inscrito.Email}) Status: {inscrito.Status} | Valor Pago: {inscrito.ValorPago} | Dt Efetivacao: {inscrito.dtEfetivacao} | Dt Criação: {inscrito.dtCriacao}\n");
        });
      return InscritosNoPacote.Count;
    }
    private static void ImprimirListaCursos()
    {
      cursos = EloreApiConsumo.RetornarCursos(eloreToken).Result.Data;
      cursos.ForEach(curso =>
      {
        Console.WriteLine($"Id: {curso.ID} | Slug: {curso.Slug} | NomeCurso: {curso.NomeCurso} - {curso.NomeTurma}");
      });
    }

    private static void ImprimirListaPacotes()
    {
      pacotes = EloreApiConsumo.RetornarPacotes(eloreToken).Result.Data;
      pacotes.ForEach(pacote =>
      {
        Console.WriteLine($"Id: {pacote.ID} | Slug: {pacote.Slug} | NomePacote: {pacote.Nome} | Total Turmas: {pacote.Turmas.Count}");
      });
    }

    private static int AdicionarTagParaInscritosNoCurso(string tag, string curso_id)
    {
      int totalAdicionado = 0;

      var cursoBuscado = EloreApiConsumo.RetornarCursoPorID(curso_id, eloreToken).Result.Data;
      if (cursoBuscado == null)
      {
        Console.WriteLine($"Curso com ID: {curso_id} não localizado.");
        return totalAdicionado;
      }

      var InscritosNoCurso = new List<InscricaoResultDTO>();

      //Iterando em todos os alunos
      var temMaisAlunos = true;
      var pagina = 1;
      do
      {
        var inscritos = EloreApiConsumo.RetornarInscritosPorCurso(curso_id, eloreToken, pagina: pagina).Result.Data;
        if (inscritos.Count > 0)
        {
          pagina++;
          inscritos.ForEach(inscrito =>
          {

            InscritosNoCurso.Add(inscrito);
          });
        }
        else
          temMaisAlunos = false;
      } while (temMaisAlunos);

      totalAdicionado = AdicionarTagParaInscritos(InscritosNoCurso, tag, curso_id);

      return totalAdicionado;
    }

    private static int AdicionarTagParaInscritosNoPacote(string tag, string pacote_id)
    {
      int totalAdicionado = 0;

      var pacoteBuscado = EloreApiConsumo.RetornarPacotePorID(pacote_id, eloreToken).Result.Data;
      if (pacoteBuscado == null)
      {
        Console.WriteLine($"Pacote com ID: {pacote_id} não localizado.");
        return totalAdicionado;
      }

      var InscritosNoPacote = RetornarTodosInscritosPorPacote(pacote_id, eloreToken);

      totalAdicionado = AdicionarTagParaInscritos(InscritosNoPacote, tag, pacote_id);

      return totalAdicionado;
    }

    private static List<InscricaoResultDTO> RetornarTodosInscritosPorPacote(string pacote_id, string eloreToken)
    {
      var InscritosNoPacote = new List<InscricaoResultDTO>();

      //Recuperando todos os alunos inscritos
      var temMaisAlunos = true;
      var pagina = 1;
      do
      {
        var inscritos = EloreApiConsumo.RetornarInscritosPorPacote(pacote_id, eloreToken, pagina: pagina).Result.Data;
        if (inscritos.Count > 0)
        {
          pagina++;
          InscritosNoPacote.AddRange(inscritos);
        }
        else
          temMaisAlunos = false;
      } while (temMaisAlunos);

      return InscritosNoPacote;
    }

    private static int RemoverTagParaInscritosNoPacote(string tag, string pacote_id)
    {
      int totalRemovido = 0;
      var pacoteBuscado = EloreApiConsumo.RetornarPacotePorID(pacote_id, eloreToken).Result.Data;

      if (pacoteBuscado == null)
      {
        Console.WriteLine($"Pacote com ID: {pacote_id} não localizado.");
        return totalRemovido;
      }

      var InscritosNoPacote = RetornarTodosInscritosPorPacote(pacote_id, eloreToken);

      totalRemovido = RemoverTagParaInscritos(InscritosNoPacote, tag, pacote_id);

      return totalRemovido;
    }

    private static int RemoverTagParaInscritos(List<InscricaoResultDTO> inscricoesDTO, string tag, string curso_id)
    {
      var totalRemovido = 0;
      inscricoesDTO.ForEach(inscricao =>
            {
              try
              {
                var result = EloreApiConsumo.RemoverTag(inscricao.UserId, tag, eloreToken).Result;
                totalRemovido++;
              }
              catch (System.Exception ex)
              {
                Console.WriteLine($"Erro ao remover tag para o aluno com ID: {inscricao.UserId}. Motivo: {ex.Message}");
              }
            });
      return totalRemovido;
    }

    private static int AdicionarTagParaInscritos(List<InscricaoResultDTO> inscricoesDTO, string tag, string curso_id)
    {
      var totalAdicionado = 0;
      inscricoesDTO.ForEach(inscricao =>
            {
              try
              {
                var result = EloreApiConsumo.AdicionarTag(inscricao.UserId, tag, eloreToken).Result;
                var tags_aluno = "";
                result.Data.ForEach(t => tags_aluno += $"{t},");
                tags_aluno = tags_aluno.Remove(tags_aluno.Length - 1);
                Console.WriteLine($"| Tags do aluno: {tags_aluno}");
                totalAdicionado++;
              }
              catch (System.Exception ex)
              {
                Console.WriteLine($"Erro ao adicionar tag para o aluno com ID: {inscricao.UserId}. Motivo: {ex.Message}");

              }
            });
      return totalAdicionado;
    }
  }
}

