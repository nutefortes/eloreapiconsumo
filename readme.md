# EloreConsumoAPI

Projeto com a finalidade de agrupar exemplos de como consumir a API do Elore.

## Importação e inscrição de alunos
###### 1 - Vá até a pasta csv_alunos e coloque o arquivo csv que deseja importar. Layout do csv: 
```
e-mail,Nome,sobrenome,CPF,estado_sigla,telefone, curso_id, curso_valor
usuario@teste.com.br, Usuario, Da Silva, , SP, 11988887777, 14070, 10.50
```

###### 2 - No arquivo **Program.cs** coloque o seu Token do Elore 

###### 3 - Após isso, já pode ser executado. Ao executar acontecerá 4 operações 
:    1 - Testar o Token (OBRIGATÓRIO)
:    2 - Ler o CSV e popular um list de alunos (OBRIGATÓRIO)
:    3 - Consultar cada aluno e verificar se eles já estão cadastrados. Caso exista um curso para inscrição, também será verificado se o curso_id existe na lista de cursos da organização. (OPCIONAL)
:    4 - Cadastrar e inscrever (caso informado) todos os alunos do csv. (OPCIONAL)
